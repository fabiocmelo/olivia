import {AlphabetWorker} from '../src/api';

describe('Test Alphabet Class', () => {
  const alphabetClass = new AlphabetWorker();

  it('should transform html to an object', () => {
    const html =
      '<html><body><table><tr><td>days</td><td>28</td></tr></body></html>';
    const obj = alphabetClass.extractHtml(html);
    expect(obj).toBeInstanceOf(Object);
  });

  it('Do a Request and return statusCode 200 or Retry Limit reached error', () => {
    return alphabetClass
      .sendRequest({url: 'http://lumtest.com/myip.json'})
      .then(response => {
        if (response instanceof Error)
          return expect(response).toBeInstanceOf(Error);
        const {status} = response;
        expect(status).toBe(200);
      });
  });
});
