# Olivia's Test

Teste de Webscrapping para a [Olivia.ai](https://www.olivia.ai/).

## Installation

```bash
npm i
```

## Usage

```typescript

npx ts-node src/start.ts

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)