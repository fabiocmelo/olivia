import {AlphabetWorker} from './api/alphabet';

((retryLimit = 5) => new AlphabetWorker(retryLimit).run().then(console.log))();
