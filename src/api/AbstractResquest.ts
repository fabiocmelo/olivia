import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import axiosCookieJarSupport from 'axios-cookiejar-support';
import debug from 'debug';
import {Tabletojson} from 'tabletojson';
import * as tough from 'tough-cookie';

axiosCookieJarSupport(axios);

/**
 * This class represents the base implementation for requests, encapsulating the cookiejar in a way so its available throw all the axios instances
 */
export abstract class AbstractRequest<T> {
  /**
   * Represents the current retry index for the request
   */
  private retry: number;

  /**
   * Cookie jar for axios
   */
  private cookieJar: tough.CookieJar;

  /**
   * The logger
   */
  protected log = debug('app');

  /**
   * Returns an instance of AbstractResquest
   * @param retryLimit the maximum of retries each request can try - default 10
   */
  constructor(public retryLimit = 10) {
    this.retry = 0;
    this.cookieJar = new tough.CookieJar();
  }

  /**
   * This method is reponsible for making the request that generate the cookies
   */
  public abstract getCookies(): Promise<Error | AxiosResponse>;

  public abstract handle: () => Promise<T>;

  public async run() {
    return this.getCookies().then(() => this.handle());
  }

  /**
   * Extracts JSON data from HTML input
   * @param {string} content page html;
   * @returns {Record<string, string>} JSON representation of the HTML input
   */
  public extractHtml(content: string): Record<string, string> {
    try {
      const obj = Tabletojson.convert(content);
      const result: Record<string, string> = {};
      //ToDo melhorar essa parte
      obj.forEach(i => {
        i.forEach(
          (row: any) =>
            (result[
              row[0].replace(/ /, '_').replace(/\//, '').toLowerCase()
            ] = `${row[1].replace(/-/, '').trim()}`)
        );
      });
      return result;
    } catch (error) {
      this.log.extend('error')(error);
      return {};
    }
  }

  /**
   * Make the request injecting the cookie jar from instance and in case of error, retry the request until the AbstractResquest.retryLimits is not reached.
   * @param {AxiosRequestConfig} requestConfig request config to be used
   * @returns {AxiosResponse|Error} the response
   */
  public async sendRequest(
    requestConfig: AxiosRequestConfig
  ): Promise<AxiosResponse | Error> {
    return axios({
      ...requestConfig,
      jar: this.cookieJar,
      withCredentials: true,
    }).catch(err => {
      this.log(err.message);
      if (this.retry >= this.retryLimit)
        throw new Error('Max retries attempt reached');

      this.retry++;

      return this.sendRequest(requestConfig);
    });
  }
}
