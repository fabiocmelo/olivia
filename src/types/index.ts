export interface AlphabetResult {
  stock: string;
  IPO: string;
  industry: string;
  data: DataResult;
  yearlyFinancials: yearlyResults[];
}

interface DataResult {
  open: string;
  high: string;
  low: string;
  mkt_cap: string;
  pe_ratio: string;
  div_yield: string;
  prev_close: string;
  '52-wk_high': string;
  '52-wk_low': string;
}

interface yearlyResults {
  year: string;
  equity: string;
  revenue: string;
  ebitda: string;
}
