"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlphabetWorker = void 0;
const _1 = require(".");
class AlphabetWorker extends _1.AbstractRequest {
    constructor() {
        super(...arguments);
        this.handle = async () => {
            const params = {
                url: 'https://fit-web-scraping-challenge.herokuapp.com/stocks/alphabet',
                method: 'GET',
                headers: {
                    Accept: 'application/json, text/javascript, */*; q=0.01',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'en-US,en;q=0.9',
                    Connection: 'keep-alive',
                    Host: 'fit-web-scraping-challenge.herokuapp.com',
                    Referer: 'https://fit-web-scraping-challenge.herokuapp.com/alphabet',
                    'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"',
                    'sec-ch-ua-mobile': '?0',
                    'sec-ch-ua-platform': '"Windows"',
                    'Sec-Fetch-Dest': 'empty',
                    'Sec-Fetch-Mode': 'cors',
                    'Sec-Fetch-Site': 'same-origin',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36',
                    'x-olivia-exam': 'alphabet-stock',
                    'X-Requested-With': 'XMLHttpRequest',
                },
            };
            return this.sendRequest(params).then(async (response) => {
                if (response instanceof Error)
                    throw response;
                const info = await this.getExtraData();
                if (info instanceof Error)
                    throw info;
                const { data } = info;
                const filteredInfo = this.extractHtml(data);
                return {
                    ...response.data,
                    data: filteredInfo,
                };
            });
        };
    }
    async getCookies() {
        const data = 'username=oliver&password=olivia';
        const headers = {
            Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9',
            'Cache-Control': 'max-age=0',
            Connection: 'keep-alive',
            'Content-Length': data.length,
            'Content-Type': 'application/x-www-form-urlencoded',
            Host: 'fit-web-scraping-challenge.herokuapp.com',
            Origin: 'https://fit-web-scraping-challenge.herokuapp.com',
            Referer: 'https://fit-web-scraping-challenge.herokuapp.com/login',
            'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36',
        };
        const url = 'https://fit-web-scraping-challenge.herokuapp.com/login';
        return this.sendRequest({
            url,
            data,
            headers,
            method: 'post',
        });
    }
    async getExtraData() {
        return this.sendRequest({
            url: 'https://fit-web-scraping-challenge.herokuapp.com/alphabet',
            method: 'GET',
            headers: {
                Accept: 'application/json, text/javascript, */*; q=0.01',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                Connection: 'keep-alive',
                Host: 'fit-web-scraping-challenge.herokuapp.com',
                Referer: 'https://fit-web-scraping-challenge.herokuapp.com/alphabet',
                'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'Sec-Fetch-Dest': 'empty',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Site': 'same-origin',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36',
                'x-olivia-exam': 'alphabet-stock',
                'X-Requested-With': 'XMLHttpRequest',
            },
        });
    }
}
exports.AlphabetWorker = AlphabetWorker;
