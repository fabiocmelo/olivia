import { AxiosResponse } from 'axios';
import { AbstractResquest, AlphabetResult } from '../types';
export declare class AlphabetWorker extends AbstractResquest<AlphabetResult> {
    retryLimit: number;
    constructor(retryLimit: number);
    handle: () => Promise<AlphabetResult>;
    objectPreparer: (obj: AxiosResponse<any> | Error, info: Partial<AlphabetResult>) => AlphabetResult;
    getCookies(): Promise<Error | AxiosResponse<any>>;
    getExtraData(): Promise<Error | AxiosResponse<any>>;
}
