"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AbstractRequest = void 0;
const axios_1 = require("axios");
const axios_cookiejar_support_1 = require("axios-cookiejar-support");
const debug_1 = require("debug");
const tabletojson_1 = require("tabletojson");
const tough = require("tough-cookie");
axios_cookiejar_support_1.default(axios_1.default);
/**
 * This class represents the base implementation for requests, encapsulating the cookiejar in a way so its available throw all the axios instances
 */
class AbstractRequest {
    /**
     * Returns an instance of AbstractResquest
     * @param retryLimit the maximum of retries each request can try - default 10
     */
    constructor(retryLimit = 10) {
        this.retryLimit = retryLimit;
        /**
         * The logger
         */
        this.log = debug_1.default('app');
        this.retry = 0;
        this.cookieJar = new tough.CookieJar();
    }
    async run() {
        return this.getCookies().then(() => this.handle());
    }
    /**
     * Extracts JSON data from HTML input
     * @param {string} content page html;
     * @returns {Record<string, string>} JSON representation of the HTML input
     */
    extractHtml(content) {
        try {
            const obj = tabletojson_1.Tabletojson.convert(content);
            const result = {};
            //ToDo melhorar essa parte
            obj.forEach(i => {
                i.forEach((row) => (result[row[0].replace(/ /, '_').replace(/\//, '').toLowerCase()] = `${row[1].replace(/-/, '').trim()}`));
            });
            return result;
        }
        catch (error) {
            this.log.extend('error')(error);
            return {};
        }
    }
    /**
     * Make the request injecting the cookie jar from instance and in case of error, retry the request until the AbstractResquest.retryLimits is not reached.
     * @param {AxiosRequestConfig} requestConfig request config to be used
     * @returns {AxiosResponse|Error} the response
     */
    async sendRequest(requestConfig) {
        return axios_1.default({
            ...requestConfig,
            jar: this.cookieJar,
            withCredentials: true,
        }).catch(err => {
            this.log(err.message);
            if (this.retry >= this.retryLimit)
                throw new Error('Max retries attempt reached');
            this.retry++;
            return this.sendRequest(requestConfig);
        });
    }
}
exports.AbstractRequest = AbstractRequest;
