"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const alphabet_1 = require("./api/alphabet");
((retryLimit = 5) => new alphabet_1.AlphabetWorker(retryLimit).run().then(console.log))();
