import { AxiosRequestConfig, AxiosResponse } from 'axios';
export declare abstract class AbstractResquest<T> {
    retryLimit: number;
    private retry;
    private cookieJar;
    constructor(retryLimit: number);
    abstract handle: () => Promise<T>;
    abstract objectPreparer: (obj: AxiosResponse<any> | Error, info: Partial<T>) => T;
    extractHtml(content: string): Record<string, string>;
    sendRequest(requestConfig: AxiosRequestConfig): Promise<AxiosResponse<any> | Error>;
}
