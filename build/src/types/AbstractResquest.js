"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AbstractResquest = void 0;
const axios_1 = require("axios");
const axios_cookiejar_support_1 = require("axios-cookiejar-support");
const tough = require("tough-cookie");
const tabletojson_1 = require("tabletojson");
axios_cookiejar_support_1.default(axios_1.default);
class AbstractResquest {
    constructor(retryLimit) {
        this.retryLimit = retryLimit;
        this.retry = 0;
        this.cookieJar = new tough.CookieJar();
    }
    extractHtml(content) {
        const obj = tabletojson_1.Tabletojson.convert(content);
        const result = {};
        //ToDo melhorar essa parte
        obj.forEach(i => {
            i.forEach((row) => (result[row[0].replace(' ', '_').replace('/', '').toLowerCase()] = `${row[1]}`));
        });
        return result;
    }
    async sendRequest(requestConfig) {
        return axios_1.default({
            ...requestConfig,
            jar: this.cookieJar,
            withCredentials: true,
        }).catch(err => {
            this.retry++;
            console.error(err.message);
            return this.retry < this.retryLimit
                ? this.sendRequest(requestConfig)
                : new Error('All retries were attempted');
        });
    }
}
exports.AbstractResquest = AbstractResquest;
//# sourceMappingURL=AbstractResquest.js.map