/* eslint-disable */
const { pathsToModuleNameMapper } = require('ts-jest/utils');
const { name, version } = require('./package.json');

module.exports = {
  name,
  displayName: `${name}@${version}`,
  preset: 'ts-jest',
  clearMocks: true,
  testEnvironment: 'node',
  moduleFileExtensions: ['js', 'json', 'jsx', 'ts', 'tsx', 'node'],
  rootDir: '.',
  collectCoverage: false,
  coverageDirectory: 'coverage',
  collectCoverageFrom: ['./packages/**/*/*.ts'],
  testTimeout: 60000,
  coverageThreshold: {
    global: {
      branches: 20,
      functions: 30,
      lines: 50,
      statements: 50,
    },
  },
  coveragePathIgnorePatterns: [
    'node_modules',
    'test-config',
    'dist',
    'interfaces',
    'jestGlobalMocks.ts',
    '.module.ts',
    '.d.ts',
    '.mock.ts',
  ],
  testEnvironment: 'node',
  testRegex: '.*(spec|test).ts$',
  testPathIgnorePatterns: ['/node_modules/', '/dist/', '/build/', '/lib/'],
};
